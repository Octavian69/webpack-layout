const path = require('path');
const UglufiPlugin = require('uglifyjs-webpack-plugin');
const HTMLPlugin = require('html-webpack-plugin');
const ExCssPlugin = require('mini-css-extract-plugin');
const OptimizeCssPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');


module.exports = {
    entry: path.join(__dirname, 'src', 'index.js'),
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    optimization: {
        minimizer: [
            new UglufiPlugin(),
            new TerserPlugin({}),
            new OptimizeCssPlugin({})
        ]
    },
    resolve: {
        extensions: ['.js', '.ts', '.jsx']
    },
    plugins: [
        new HTMLPlugin({
            filename: 'index.html',
            template: path.join(__dirname, 'src', 'index.html')
        }),
        new ExCssPlugin({
            filename: 'index.css',
        })
    ],
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'),
        port: 4200
    },
    module: {
        rules: [
            {
                test: /\.(jsx?|ts)$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env',
                            '@babel/preset-react',
                            '@babel/preset-typescript'
                        ]
                    }
                }
            },
            {
                test: /\.less$/,
                use: [
                    {
                        loader: ExCssPlugin.loader
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'less-loader'
                    }
                ] 
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: ExCssPlugin.loader
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            }
        ]
    }
}