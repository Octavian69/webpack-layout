interface IUser {
    _id: string;
    name: string;
    age: number;
};

const user: IUser = {
    _id: '$hrsj441hda',
    name: 'Harold Harrison',
    age: 37
}

console.log(user);