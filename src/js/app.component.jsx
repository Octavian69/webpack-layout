import React from 'react';

export default class App extends React.Component {

    render() {
        return (
            <React.Fragment>
                <header className="header">
                    <h1 className="header-text">
                        Hello webpack practice!
                    </h1>
                </header>

                <main className="main">
                    <h2 className="main-text">
                        Add SCSS loader and compiler!
                    </h2>
                </main>
            </React.Fragment>
        )
    }
}