//Libs
import React from 'react';
import { render } from 'react-dom';

//components
import App from './js/app.component';

//interfaces
import './ts/IUser';

//styles
import './styles/less/index.less';
import './styles/scss/index.scss';

render(<App />, document.getElementById('root'));